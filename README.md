![youtube banner](https://frankiezafe.org/images/c/c0/Realtime_3D_object%27s_texture_modification_in_openframeworks.jpg)

page: [frankiezafe.org](https://frankiezafe.org/w/Realtime_3D_object%27s_texture_modification_in_openframeworks)

author : françois zajéga - frankie@frankiezafe.org
release date: 28/09/2013

# resources:
############

* prog
** http://www.machwerx.com/2009/02/11/glblendfunc/
** http://www.songho.ca/opengl/gl_fbo.html

* images
** resource1.jpg : image by Atelier Olschinsky -> http://www.behance.net/olschinsky
** resource2.jpg : copyright Franz Schumacher -> http://1x.com/photo/35536

* model 3D
** NewSquirrel.3ds -> see data/squirrel/readme.txt for licence details

# about
#######

This code runs in OpenFrameworks v0.7.4, not tested in other release.
It uses a hack of the addons "ofx3DModelLoader", that's why the code has been 
added in the project.

# how it works
##############

1_ Each image resource is stored in an AlphaResource struct. 
AlphaResource has 2 textures. An alpha mask (b&w) and the result of the mask 
applied on the image, called "mixture". [ testApp.h, line 14 ]

2_ To render the final texture, all resources and alpharesources are mixed in an
ofFBO. [ testApp.cpp, line 104 ]

3_ The pixels of the final textures are copied into a MipMaps texture on the GPU
level by the "gluBuild2DMipmaps" method. [ testApp.cpp, line 122 ]

4_ The last operation is to switch the texture ID stored in the 3D model via the
custom method "switchAllMaterialTextureID". In the genuine addon, it was 
impossible to do that, for obvious consistency reasons :) 
[ testApp.cpp, line 124 & 3DS/model3DS.cpp, line 685 ]

# Attached license
##################

A license is attached to the code in "src" folder and base.jpg.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailled license : http://creativecommons.org/licenses/by/2.5/

