#pragma once

#include "ofMain.h"
#include "ofx3DModelLoader.h"

struct Resource {
    ofImage * img;
    int w;
    int h;
    int pixelcount;
    unsigned char * pixels;
};

struct AlphaResource {
    ofImage * img;
    int w;
    int h;
    int pixelcount;
    unsigned char * mask_data;
    unsigned char * mix_data;
    ofTexture * mask;
    ofTexture * mixture;
    int opacity;
};

class testApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
                void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
                
        private:
            
            int strength;
            int radius;
            
            ofFbo * tex;
            
            Resource * base;
            AlphaResource * resource1;
            AlphaResource * resource2;

            void buildResource( Resource * r, string path );
            void buildAlphaResource( AlphaResource * ar, string path, int opacity = 255 );
            void resetAlphaResource( AlphaResource * ar, int alpha = 0 );
            
            void drawInAlpha( int x, int y, int radius, int strength, AlphaResource * ar );
            
            // 3D
            ofPoint rot;
            GLuint modelTexture;
            ofx3DModelLoader model;
            
};
