#include "testApp.h"


GLfloat lightOnePosition[] = {40.0, 40, 100.0, 0.0};
GLfloat lightOneColor[] = {0.99, 0.99, 0.99, 1.0};

GLfloat lightTwoPosition[] = {-40.0, 40, 100.0, 0.0};
GLfloat lightTwoColor[] = {0.99, 0.99, 0.99, 1.0};

//--------------------------------------------------------------
void testApp::setup(){
    
    tex = new ofFbo();
    tex->allocate( 512, 512, GL_RGBA );
    tex->begin();
    ofClear( 0,0,0,0 );
    tex->end();
    
    base = new Resource();
    resource1 = new AlphaResource();
    resource2 = new AlphaResource();
    
    buildResource( base, "base.jpg" );
    buildAlphaResource( resource1, "resource1.jpg", 200 );
    buildAlphaResource( resource2, "resource2.jpg", 150 );
    
    strength = 15;
    radius = 80;
    
    glGenTextures(1, &modelTexture);
    
    glLightfv (GL_LIGHT0, GL_POSITION, lightOnePosition);
    glLightfv (GL_LIGHT0, GL_DIFFUSE, lightOneColor);
    glEnable (GL_LIGHT0);
    glLightfv (GL_LIGHT1, GL_POSITION, lightTwoPosition);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, lightTwoColor);
    glEnable (GL_LIGHT1);
    glColorMaterial (GL_FRONT_AND_BACK, GL_DIFFUSE);
    glEnable (GL_COLOR_MATERIAL);
    model.loadModel("squirrel/NewSquirrel.3ds", 20);
    model.setPosition(ofGetWidth()/2, ofGetHeight()/2, 0);
    
    cout << "GLuint textureId: " << tex->getTextureReference().getTextureData().textureID << "\n" <<
            "GLuint MipMap: " << modelTexture << endl;

    
}

void testApp::buildResource( Resource * r, string path ) {
    
    r->img = new ofImage();
    r->img->loadImage( path );
    r->img->setImageType( OF_IMAGE_COLOR );
    r->w = r->img->getWidth();
    r->h = r->img->getHeight();
    r->pixelcount = r->w * r->h;
    r->pixels = r->img->getPixels();
    
}

void testApp::buildAlphaResource( AlphaResource * ar, string path, int opacity ) {
    
    ar->opacity = opacity;
    ar->img = new ofImage();
    ar->img->loadImage( path );
    ar->w = ar->img->getWidth();
    ar->h = ar->img->getHeight();
    ar->pixelcount = ar->w * ar->h;
    unsigned char * img_px = ar->img->getPixels();
    ar->mask_data = new unsigned char[ ar->pixelcount ];
    ar->mix_data = new unsigned char[ ar->pixelcount * 4 ];
    for ( int i = 0; i < ar->pixelcount; i++ ) {
        ar->mask_data[ i ] = 0;
        ar->mix_data[ ( i * 4 ) ] = img_px[ ( i * 3 ) ];
        ar->mix_data[ ( i * 4 ) + 1 ] = img_px[ ( i * 3 ) + 1 ];
        ar->mix_data[ ( i * 4 ) + 2 ] = img_px[ ( i * 3 ) + 2 ];
        ar->mix_data[ ( i * 4 ) + 3 ] = 0;
    }
    ar->mask = new ofTexture();
    ar->mask->allocate( ar->w, ar->h, GL_LUMINANCE );
    ar->mask->loadData( ar->mask_data, ar->w, ar->h, GL_LUMINANCE );
    ar->mixture = new ofTexture();
    ar->mixture->allocate( ar->w, ar->h, GL_RGBA );
    ar->mixture->loadData( ar->mix_data, ar->w, ar->h, GL_RGBA );
    
    
}

void testApp::resetAlphaResource( AlphaResource * ar, int alpha ) {
    
    for ( int i = 0; i < ar->pixelcount; i++ ) {
        ar->mask_data[ i ] = alpha;
        ar->mix_data[ ( i * 4 ) + 3 ] = alpha;
    }
    ar->mask->loadData( ar->mask_data, ar->w, ar->h, GL_LUMINANCE );
    ar->mixture->loadData( ar->mix_data, ar->w, ar->h, GL_RGBA );
    
}

//--------------------------------------------------------------
void testApp::update(){
    
    // final update
    tex->begin();
        ofEnableAlphaBlending();
        base->img->draw( 0,0 );
        ofSetColor( 255,255,255, resource1->opacity );
        resource1->mixture->draw( 0,0 );
        ofSetColor( 255,255,255, resource2->opacity );
        resource2->mixture->draw( 0,0 );
    tex->end();
    
    glBindTexture(GL_TEXTURE_2D, modelTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    ofPixels texpx;
    tex->getTextureReference().readToPixels( texpx );
    gluBuild2DMipmaps(
            GL_TEXTURE_2D, GL_RGB, tex->getWidth(), tex->getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, 
            texpx.getPixels() );
    model.switchAllMaterialTextureID( modelTexture );

}

void testApp::drawInAlpha( int x, int y, int radius, int strength, AlphaResource * ar ) {

    for ( int r = y - radius; r <= y + radius; r++ ) {
        for ( int c = x - radius; c <= x + radius; c++ ) {
            if ( c < 0 || c >= ar->w || r < 0 || r >= ar->h )
                continue;
            float d = ofDist( c, r, x, y );
            d /= radius;
            if ( d >= 1 )
                continue;
            int p = c + r * ar->w;
            int alpha = (int) ( ( 1-d ) * strength );
            int r = ar->mask_data[ p ] + alpha;
            if ( r > 255 )
                r = 255;
            if ( r < 0 )
                r = 0;
            ar->mask_data[ p ] = r;
            ar->mix_data[ ( p * 4 ) + 3 ] = r;
        }
    }
    ar->mask->loadData( ar->mask_data, ar->w, ar->h, GL_LUMINANCE );
    ar->mixture->loadData( ar->mix_data, ar->w, ar->h, GL_RGBA );

}

//--------------------------------------------------------------
void testApp::draw(){


    ofBackground( 80,80,80 );
    
    ofSetColor( 255,255,255,255 );
    tex->draw( 0,0 );
    
    ofEnableAlphaBlending();
    
    resource1->mask->draw( 512, 0, 256, 256 );
    resource1->mixture->draw( 512, 256, 256, 256 );
    ofDrawBitmapString( "opacity: " + ofToString( resource1->opacity ), 522, 502 );
    
    resource2->mask->draw( 768, 0, 256, 256 );
    resource2->mixture->draw( 768, 256, 256, 256 );
    ofDrawBitmapString( "opacity: " + ofToString( resource2->opacity ), 778, 502 );
    
//    roitex->draw( 1024, 0, 256, 256 );
    
    ofDrawBitmapString( "fps: " + ofToString( ofGetFrameRate() ),10, 527 );
    ofDrawBitmapString( "strength: " + ofToString( strength ), 10, 542 );
    ofDrawBitmapString( "radius: " + ofToString( radius ), 10, 557 );
    
    glEnable (GL_LIGHTING);
    glEnable (GL_DEPTH_TEST);
    glShadeModel (GL_SMOOTH);
    glPushMatrix();

        //draw in middle of the screen
        glTranslatef(ofGetWidth()/2,ofGetHeight()/2, 300);
        //tumble according to mouse
        glRotatef(rot.y,1,0,0);
        glRotatef(rot.x,0,1,0);
        glTranslatef(-ofGetWidth()/2,-ofGetHeight()/2,0);
        
        ofSetColor(255, 255, 255, 255);
        model.draw();

    glPopMatrix();
    glDisable (GL_DEPTH_TEST);
    glDisable (GL_LIGHTING);
    
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    
    if ( key == 'r' ) {
        
        resetAlphaResource( resource1 );
        resetAlphaResource( resource2 );
        
    } else if ( key == 357 ) {
    
        strength+=5;
        if ( strength > 255 )
            strength = 255;
        
    } else if ( key == 359 ) {
    
        strength-=5;
        if ( strength < -255 )
            strength = -255;
        
    } else if ( key == 358 ) {
    
        radius+=5;
        
    } else if ( key == 356 ) {
    
        radius-=5;
        if ( radius < 0 )
            radius = 0;
        
    } else {
    
        cout << key << endl;
        
    }

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

    switch ( button ) {
    
        case 0:
            // drawing on texture
            if ( x > 512 && x < 768 && y > 0 && y < 256  )
                drawInAlpha( ( ( x - 512 ) * 2 ), ( y * 2 ), radius, strength, resource1 );
            else if ( x > 768 && x < 1024 && y > 0 && y < 256  )
                drawInAlpha( ( ( x - 768 ) * 2 ), ( y * 2 ), radius, strength, resource2 );
            break;
        
        case 2:
            rot.x = x;
            rot.y = y;
            break;
        
    }
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}